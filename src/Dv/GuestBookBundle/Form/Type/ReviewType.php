<?php

namespace Dv\GuestBookBundle\Form\Type;

use Dv\TaskBundle\Form\Type\AuthorType;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ReviewType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('author', new AuthorType());
        $builder->add('website', new WebSiteType());
        $builder->add('comment', new CommentType());
        $builder->add('rating', new RatingType());
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Dv\TaskBundle\Entity\GuestBook'
        );
    }

    public function getName()
    {
        return 'review';
    }
}