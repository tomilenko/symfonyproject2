<?php

namespace Dv\GuestBookBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class WebSiteType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('website');
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Dv\TaskBundle\Entity\WebSite',
        );
    }

    public function getName()
    {
        return 'website';
    }
}