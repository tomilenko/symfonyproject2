<?php

namespace Dv\TaskBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class AuthorType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('author');
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Dv\TaskBundle\Entity\Author',
        );
    }

    public function getName()
    {
        return 'author';
    }
}