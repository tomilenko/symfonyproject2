<?php

namespace Dv\GuestBookBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class RatingType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('rating');
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Dv\TaskBundle\Entity\Rating',
        );
    }

    public function getName()
    {
        return 'rating';
    }
}