<?php

namespace Dv\GuestBookBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class DateType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('date');
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Dv\TaskBundle\Entity\Date',
        );
    }

    public function getName()
    {
        return 'date';
    }
}