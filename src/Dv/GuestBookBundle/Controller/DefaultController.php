<?php

namespace Dv\GuestBookBundle\Controller;

use Dv\GuestBookBundle\Entity\Guestbook;
use Monolog\ErrorHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Dv\GuestBookBundle\Form\Type\ReviewType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $reviews = $this->getDoctrine()
            ->getRepository('DvGuestBookBundle:Guestbook')
            ->findBy(array(), array('id' => 'DESC'));

        return $this->render(
            'DvGuestBookBundle:Default:index.html.twig',
            array(
                'form'    => $this->getGuestBookForm()->createView(),
                'reviews' => $reviews
            )
        );
    }


    public function postAction(Request $request)
    {
        $form = $this->getGuestBookForm();
        $form->handleRequest($request);

        $response = array(
            'error'    => false,
            'messages' => array('Firm Submitted successfully')
        );
        if ($form->isValid()) {
            $guestBook = $form->getData();
            $guestBook->setRating($guestBook->getRating() + 1)
                ->setDate(new \DateTime())
                ->setWebsite(str_replace("http://", " ", $guestBook->getWebsite()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($guestBook);
            $em->flush();
        } else {
            $response = array(
                'error'    => true,
                'messages' => array('Error submit review')
            );
        }
        return new Response(json_encode($response));
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function getGuestBookForm()
    {
        $guestBook = new Guestbook();
        $form = $this->createFormBuilder($guestBook)
            ->setAction($this->get('router')->generate('guest_book_review'))
            ->add('author', 'text')
            ->add('website', 'url')
            ->add('comment', 'textarea')
            ->add('rating', 'choice',
                array(
                    'expanded' => true,
                    'choices' => array(
                        '1' => '☆',
                        '2' => '☆',
                        '3' => '☆',
                        '4' => '☆',
                        '5' => '☆',
                        '6' => '☆',
                        '7' => '☆',
                        '8' => '☆',
                        '9' => '☆',
                        '10' => '☆'
                    )
                ))
            ->add('save', 'submit', array(
                  'label' => 'Post Review'
            ))
            ->getForm();
        return $form;
    }


    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Dv\GuestBookBundle\Entity\GuestBook',
        );
    }

    public function newAction()
    {
        $guestbook = new Guestbook();
        $form = $this->createFormBuilder($guestbook)
            ->add('author', array(
                'min_length' => 6,
                'max_length' => 255
            ))
            ->getForm();

        return $this->render('DvGuestBookBundle:Default:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
