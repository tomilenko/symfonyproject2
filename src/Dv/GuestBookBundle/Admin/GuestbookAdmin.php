<?php
namespace Dv\GuestBookBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class GuestbookAdmin extends Admin
{

    /**
     * Fields to be shown on create/edit forms
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields( FormMapper $formMapper ) {
        $formMapper
            ->add( 'author', 'text', array( 'label' => 'Author' ) )
            ->add( 'date', 'date', array( 'label' => 'Date' ) )
            ->add( 'website', 'text', array( 'label' => 'Website' ) )
            ->add( 'comment', 'text', array( 'label' => 'Comment' ) )
            ->add( 'rating', 'integer', array( 'label' => 'Rating' ) )
        ;
    }

    /**
     * Fields to be shown on filter forms
     *
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters( DatagridMapper $datagridMapper ) {
        $datagridMapper
            ->add( 'author' )
            ->add( 'date' )
            ->add( 'website' )
            ->add( 'comment' )
            ->add( 'rating' )
        ;
    }

    /**
     * Fields to be shown on lists
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields( ListMapper $listMapper ) {
        $listMapper
            ->addIdentifier( 'author' )
            ->add( 'website' )
            ->add( 'date' )
            ->add( 'comment' )
            ->add( 'rating' )
        ;
    }


}