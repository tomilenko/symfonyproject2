$(document).ready(function() {
    var $form = $('form.form');

    $form.submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: ('POST'),
            url: $form.attr("action"),
            data: $form.serialize(),
            success: function() {
                $('form.form')[0].reset();
            }
        });
        alert('Review submitted!');
        return false;
    });
});